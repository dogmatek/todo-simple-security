package ru.rkmv.todo.repository;

import ru.rkmv.todo.domain.ToDo;
import org.springframework.data.repository.CrudRepository;

public interface ToDORepository extends CrudRepository <ToDo,String> {
}
